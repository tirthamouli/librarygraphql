const express = require("express");
const graphqlHTTP = require("express-graphql"); //Middle ware for express
const schema = require("./schema/schema");
const mongoose = require("mongoose");
const cors = require("cors");

// For heroku
var PORT = process.env.PORT || 4000;

const app = express();

//Allow cross origin request
app.use(cors());

// Connect to database
mongoose.connect(
  "mongodb://tirth:password09%23@ds255463.mlab.com:55463/library",
  { useNewUrlParser: true }
);

mongoose.connection.once("open", () => {
  console.log("Connected successfully");
});

app.use("/", express.static("build"));
app.use("/details/*", express.static("build"));
app.use("/add", express.static("build"));

app.use(
  "/graphql",
  graphqlHTTP({
    schema,
    graphiql: true
  })
);

app.listen(PORT, () => {
  console.log("Server listening to port 4000");
});
